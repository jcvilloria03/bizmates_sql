-=-=-
Question1:
SELECT CONCAT('T', LPAD(t.id, 11, 0)) AS ID, t.nickname AS Nickname, REPLACE(REPLACE(REPLACE(t.status, '0', 'Discontinued') , '1', 'Active') , '2', 'Deactivated') AS Status, REPLACE(REPLACE(REPLACE(GROUP_CONCAT(DISTINCT r.role SEPARATOR'/'), '1', 'Trainer') , '2', 'Assessor') , '3', 'Staff') AS Roles
FROM trn_teacher t
JOIN trn_teacher_role r ON t.id = r.teacher_id
GROUP BY t.id
-=-=-
Question2:
SELECT t.id AS ID, t.nickname AS Nickname, COUNT(DISTINCT(tt.id)) AS Open, 
(SELECT IFNULL(( SELECT COUNT(DISTINCT(tt.id)) AS Reserved
FROM trn_teacher t
JOIN trn_teacher_role r ON t.id = r.teacher_id
JOIN trn_time_table tt ON t.id = tt.teacher_id
JOIN trn_evaluaation e ON t.id = e.teacher_id
WHERE t.status IN (1,2)
AND r.role IN (1,2)
AND tt.status = 3
AND e.result = 1
GROUP BY t.id
HAVING COUNT(DISTINCT r.role) = 2),0)) AS Reserved, t2.Taught,
(SELECT IFNULL(( SELECT COUNT(DISTINCT(tt.id)) AS NoShow
FROM trn_teacher t
JOIN trn_teacher_role r ON t.id = r.teacher_id
JOIN trn_time_table tt ON t.id = tt.teacher_id
JOIN trn_evaluaation e ON t.id = e.teacher_id
WHERE t.status IN (1,2)
AND r.role IN (1,2)
AND tt.status = 1
AND e.result = 2
GROUP BY t.id
HAVING COUNT(DISTINCT r.role) = 2),0)) AS NoShow

FROM trn_teacher t
JOIN trn_teacher_role r ON t.id = r.teacher_id
JOIN trn_time_table tt ON t.id = tt.teacher_id
JOIN trn_evaluaation e ON t.id = e.teacher_id
JOIN
(SELECT t.id aid,   COUNT(DISTINCT(tt.id)) AS Taught
FROM trn_teacher t
JOIN trn_teacher_role r ON t.id = r.teacher_id
JOIN trn_time_table tt ON t.id = tt.teacher_id
JOIN trn_evaluaation e ON t.id = e.teacher_id
WHERE t.status IN (1,2)
AND r.role IN (1,2)
AND tt.status = 1
AND e.result = 1
GROUP BY t.id
HAVING COUNT(DISTINCT r.role) = 2

) t2
ON  
t.id = t2.aid
WHERE t.status IN (1,2)
AND r.role IN (1,2)
AND tt.status = 1
AND e.result = 1
GROUP BY t.id
HAVING COUNT(DISTINCT r.role) = 2
-=-=-